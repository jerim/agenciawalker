<?php

use Illuminate\Database\Seeder;

class ProductosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        DB::table('productos')->insert([
            'id'=> 2,
            'nombre'=>'dhdfhfd',
            'descripcion'=> "idfhfdhdl duseib",
            'origen'=>'lulu',
            'numPersona'=>5,
            'rutaImg'=>"OLIMPIA.JPG",
            'precio'=>2500,
            'categoria_id'=> 2
        ]);

        DB::table('productos')->insert([
            'id'=> 3,
            'nombre'=>'dhUUhfd',
            'descripcion'=> "idfhfdhdl duseib",
            'origen'=>'ddeeru',
            'numPersona'=>9,
            'rutaImg'=>"Monaco.JPG",
            'precio'=>2900,
            'categoria_id'=> 3
        ]);

        DB::table('productos')->insert([
            'id'=> 4,
            'nombre'=>'dhUUhfd',
            'descripcion'=> "iiiiidhdl duseib",
            'origen'=>'dttteru',
            'numPersona'=>2,
            'rutaImg'=>"GEIRANGER.JPG",
            'precio'=>2900,
            'categoria_id'=> 4
        ]);

        DB::table('productos')->insert([
            'id'=> 5,
            'nombre'=>'dhUUhfd',
            'descripcion'=> "iiiiidhdl duseib",
            'origen'=>'dttteru',
            'numPersona'=>6,
            'rutaImg'=>"CIMG0517.JPG",
            'precio'=>900,
            'categoria_id'=> 5
        ]);

        DB::table('productos')->insert([
            'id'=> 1,
            'nombre'=>'dhUUhfd',
            'descripcion'=> "iiiiidhdl duseib",
            'origen'=>'dttteru',
            'numPersona'=>6,
            'rutaImg'=>"Florencia.JPG",
            'precio'=>900,
            'categoria_id'=> 1
        ]);
    }
}
