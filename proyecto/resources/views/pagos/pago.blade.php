@extends('layouts.app')

@section('content')
  <div class="card text-center">
      <div class="card-header" style="background-color: #333; color:white;">Estatus de pago</div>
      <div class="card-body">
        <h5 class="card-title">El pago ha sido completado</h5>
        <p class="card-text">Usted si quiere puede imprimir la factura en pdf o enviarlo por email.</p>
        <a href="#" class="btn btn-primary">Factura PDF</a>
        <a href="/email" class="btn btn-primary">Envio por email</a>
      </div>
      <div class="card-footer text-muted" style="background-color: #333; color:white;"><a href="/agencia" class="btn" style="background-color: #3b83bd; color:white;">Volver a descubre</a></div>
</div>
@endsection
