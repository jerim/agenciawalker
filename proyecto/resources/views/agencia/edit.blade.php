@extends('layouts.content')
@section('content')
<div class="container" style="margin-left:-10px">
  <div class="row justify-content-center">
      <div class="col-md-12">
         <h1> Editar producto </h1>
         <form method="POST" action="/agencia/{{$producto->id}}" enctype="multipart/form-data">
            {{csrf_field()}}

            <input type="hidden" name="_method" value="PUT">

            <div class="form-group">
                <label>Nombre</label>
                <input class="form-control" type="text" name="nombre" value="{{$producto->nombre}}" >
                @if($errors->any('nombre'))
                  <span class="text-danger">{{$errors->first('nombre')}}</span>
                @endif
            </div>

             <div class="form-group">
                <label>Origen</label>
                <input class="form-control" type="text" name="origen" value="{{$producto->origen}}" >
                @if($errors->any('origen'))
                  <span class="text-danger">{{$errors->first('origen')}}</span>
                @endif
            </div>

            <div class="form-group">
                <label>Descripcion</label>
                <input class="form-control" type="text" name="descripcion" value="{{$producto->descripcion}}" >
                @if($errors->any('descripcion'))
                  <span class="text-danger">{{$errors->first('descripcion')}}</span>
                @endif
            </div>

            <div class="form-group">
                <label>Número de personas</label>
                <input class="form-control" type="Integer" name="numPersona" value="{{$producto->numPersona}}">
                @if($errors->any('numPersona'))
                  <span class="text-danger">{{$errors->first('numPersona')}}</span>
                @endif
            </div>


            <div class="form-group">
                <label>Precio</label>
                <input class="form-control" type="Integer" name="precio" value="{{$producto->precio}}">
                @if($errors->any('precio'))
                  <span class="text-danger">{{$errors->first('precio')}}</span>
                @endif
            </div>

            <div class="form-group">
              <label>Categoria</label>
              <select class="form-control" name="categoria_id" value="categoria_id">
                @foreach($categorias as $categoria)
                  <option value="{{$categoria->id}}" {{old('categoria') == $categoria ? 'selected="selected"':''}}>
                    {{$categoria->nombre}}
                  </option>
                @endforeach
              </select>
               @if($errors->any('categoria_id'))
                  <span class="text-danger">{{$errors->first('categoria_id')}}</span>
              @endif
            </div>


            <div class="form-group">
                <label>Imagen</label>
                <div class="card" style="width:18rem;">
                 <img src="/imagenes/productos/{{$producto->rutaImg}}" class="card-img-top">
                 </div>
                <input accept="image/*" type="file" name="file" value='file'>
                 @if($errors->any('file'))
                  <span class="text-danger">{{$errors->first('file')}}</span>
              @endif
            </div>


            <input type="submit" class="btn btn-success" name="Editar" value="Editar">
            <a href="/agencia" class="btn btn-success">Volver a home</a>
         </form>



      </div><!--col md -->
    </div><!--col justify -->
</div> <!--container -->
@endsection
