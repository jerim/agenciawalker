@extends('layouts.app')

@section('content')
    <div class="row">
    <div class="col-sm-6">
     <h1>Cesta de la compra</h1>
      <table class="table table-hover">
        <thead>
          <tr>
            <th>Nombre</th>
            <th>Cantidad</th>
            <th>Precio</th>
            <th>Añadir</th>
            <th>Quitar</th>
          </tr>
        </thead>
        <tbody>
           @forelse($cesta as $producto)
           <tr>
              <td>{{$producto->nombre}}</td>
              <td>{{$producto->cantidad}}</td>
              <td>{{$producto->precio}} €</td>
              <td><a href="/tienda/{{$producto->id}} " class="badge badge-danger badge-pill"> + </a></td>
              <td><a href="/tienda/quitar/{{$producto->id}} " class="badge badge-danger badge-pill"> - </a></td>
           </tr>

           @empty
           <tr>
             <td colspan="5">¡No hay productos en la cesta!</td>
           </tr>
         @endforelse
        </tbody>
      </table>

          <a href="/tienda/vaciar" class="btn btn-danger">Eliminar la cesta</a>

     </div>

  <div class="col-sm-6" style="margin-top: 55px;">
      <div class="card bg-light mb-3" style="max-width: 40rem;">
        <div class="card-header">Total a pagar:</div>
      </div>
      <div class="card-body">
        <strong>{{$total}} €</strong>
      </div>
      <div class="card-footer"style="max-width: 40rem; ">

        <a href="/comprar" class="btn btn-success">Comprar</a>
        <a href="#" class="btn btn-success">Presuesto</a>
      </div>
  </div>


  </div>

@endsection
