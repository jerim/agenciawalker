<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TarjetaVisa extends Model
{
    protected $table= 'tarjetaVisa';
    protected $fillable = ['nombre','apellidos','email','direccion','ciudad','numTarjeta','numExpiracion','numCsv'];
}
