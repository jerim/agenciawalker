<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TarjetaVisa;

class TarjetaVisaController extends Controller
{
    public function index() {
        $tarjeta=TarjetaVisa::all();
        return view('tarjetaVisa.index',['tarjeta'=>$tarjeta]);
    }

    public function create() {
       $tarjeta=TarjetaVisa::all();
       return view('tarjetaVisa.create',['tarjeta'=>$tarjeta]);
    }


    public function store(Request $request) {
        $rules = [
            'nombre' => 'required|max:255|min:3',
            'apellidos' => 'required|max:255|min:3',
            'email' => 'required|max:255|min:3',
            'direccion' =>  'required|max:255|min:3',
            'ciudad' => 'required|max:255|min:3',
            'numTarjeta'=>'required|numeric',
            'numExpiracion'=>'required|numeric',
            'numCsv'=>'required|numeric',
        ];

        $messages= [
            'required'=>'Los campos son obligatorios',
            'max'=>'Máximo 255 caracteres.',
            'min'=>'Minimo debe ser tres números',
            'numeric'=>'Debe ser un caracter númerico',
        ];

        $request->validate($rules,$messages);

        $tarjeta = new TarjetaVisa();
        $tarjeta->fill($request->all());
        $tarjeta->save();
        return redirect("/tarjetaVisa");
    }


     public function destroy($id)
    {
        $tarjeta = TarjetaVisa::destroy($id);
        return redirect("/tienda");
    }
}
